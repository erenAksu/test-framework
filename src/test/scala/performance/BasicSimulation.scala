package performance

import io.gatling.core.Predef._
import io.gatling.http.Predef._
//import io.gatling.http.config.HttpProtocolBuilder.toHttpProtocol
import io.gatling.http.request.builder.HttpRequestBuilder.toActionBuilder

class BasicSimulation extends Simulation{



  val httpConf = http
    .baseUrl("http://otomasyonlisans:18084")
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")


  val scn = scenario("BasicSimulation")
    .exec(http("request_1")
      .post("/fim/api")
      .body(StringBody("""<CC5Request>
                         |<Name>apiuser</Name>
                         |<Password>TEST1234</Password>
                         |<ClientId>601616166</ClientId>
                         |<Type>PreAuth</Type>
                         |<Total>10.15</Total>
                         |<Currency>978</Currency>
                         |<Number>4242424242424242</Number>
                         |<Expires>10/2028</Expires>
                         |<Cvv2Val>000</Cvv2Val>
                         |</CC5Request> """.stripMargin)).asXml)//was .asJson
    .pause(5)

  setUp(
    scn.inject(atOnceUsers(100)) // 12
  ).protocols(httpConf)


}
