package driverclasses;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriverFactory {

    public static WebDriver getWebDriver() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\eren.aksu\\IdeaProjects\\test-framework\\target\\test-classes\\binaries\\chromedriver.exe");
        return new ChromeDriver();
    }

}
