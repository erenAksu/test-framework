package stepdefinitions;

import driverclasses.DriverFactory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class AmazonSearchFunctionStepDef extends DriverFactory {

    public WebDriver browser;

    public AmazonSearchFunctionStepDef() {
        browser = getWebDriver();
    }

    @Given("I navigated to Amazon Page")
    public void iNavigatedToAmazonPage() throws Throwable {
        System.out.println("UI TESTI BASLADI");
        browser.navigate().to("https://www.amazon.com/");
        browser.manage().window().maximize();
    }

    @When("I write {string} to search textbox")
    public void iWriteToSearchTextbox(String arg0) throws Throwable {

        WebElement element = browser.findElement(By.id("twotabsearchtextbox"));
        element.sendKeys(arg0);
        browser.findElement(By.xpath("//*[@id=\"nav-search-submit-text\"]/input")).click();

    }

    @Then("I see website has a response")
    public void iSeeWebsiteHasAResponse() throws Throwable {
        WebElement element = browser.findElement(By.xpath("//*[@id=\"search\"]/span/div/span/h1/div/div[1]/div/div/span[1]"));
        String elementText = element.getText();
        boolean isContains = elementText.contains("results");
        Assert.assertTrue("there is no result", isContains);
        browser.close();

    }

    @When("I write appleappleapple to search textbox")
    public void iWriteAppleappleappleToSearchTextbox() throws Throwable {
        WebElement element = browser.findElement(By.id("twotabsearchtextbox"));
        element.sendKeys("appleappleapple");
        browser.findElement(By.xpath("//*[@id=\"nav-search-submit-text\"]/input")).click();
    }

    @Then("I see website has nothing to show")
    public void iSeeWebsiteHasNothingToShow() throws Throwable {
        WebElement element = browser.findElement(By.xpath("//*[@id=\"search\"]/div[1]/div[2]/div/span[3]/div[2]/div[1]/span/div/div/div[1]/span[1]"));
        String elementText = element.getText();
        boolean isContains = elementText.contains("No results for");
        Assert.assertFalse("there is no result",!isContains);
        browser.close();
    }

    @When("I write nothing to search textbox")
    public void iWriteNothingToSearchTextbox() throws Throwable {
        WebElement element = browser.findElement(By.id("twotabsearchtextbox"));
        element.sendKeys("");
        browser.findElement(By.xpath("//*[@id=\"nav-search-submit-text\"]/input")).click();
    }

    @Then("I see website has nothing to show and redirected to home page")
    public void iSeeWebsiteHasNothingToShowAndRedirectedToHomePage() throws Throwable {
        WebElement element = browser.findElement(By.id("desktop-banner-stripe"));
        String elementText = element.getText();
        boolean isContains = elementText.contains("amazon");
        Assert.assertFalse("there is no result",!isContains);
        browser.close();
        browser.quit();
    }


}
