package stepdefinitions;

import java.util.List;
import java.util.Map;
import java.util.Random;

import org.junit.Assert;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class FakeRestApiTestStepDef {
    private static final int ID = 1616161616;
    private static final int ID2 = 16321;
    private static final String userName = "ErenAksu";
    private static final String passWord = "123456";
    private static final String BASE_URL = "http://fakerestapi.azurewebsites.net";

    private static Response response;
    private static String jsonString;

    @Given("I send post request to define a user and I see that user has been registered")
    public void iSendPostRequestToDefineAUser() throws Throwable {
        RestAssured.baseURI = BASE_URL;
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        Random random = new Random();
        int upperBound = 4000;
        int randomId = random.nextInt(upperBound);
        response = request.body("{\n" +
                "  \"ID\":" + randomId + ",\n" +
                "  \"UserName\": \"" + userName + "\",\n" +
                "  \"Password\": \"" + passWord + "\"\n" +
                "}").post("/api/users");
        String responseBody = response.getBody().asString();
        //String responseBody = response.toString();
        boolean isContains = responseBody.contains(userName);
        Assert.assertTrue("User has not registered!", isContains);
    }

    @Given("I send get request to have info of registered user")
    public void iSendGetRequestToHaveInfoOfRegisteredUser() throws Throwable {
        RestAssured.baseURI = BASE_URL;
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        //RestAssured.given().baseUri(BASE_URL).pathParam("id",1).get("api/users/{id}");
        response = request.given().baseUri(BASE_URL).pathParam("id",1).get("api/users/{id}");
        String responseBody = response.getBody().asString();
        boolean isContains = responseBody.contains("User 1");
        Assert.assertTrue("User info cannot be seen!", isContains);
    }

    @Given("I send get request by using id that does not exist")
    public void iSendGetRequestByUsingIdThatDoesNotExist() throws Throwable {
        RestAssured.baseURI = BASE_URL;
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        //RestAssured.given().baseUri(BASE_URL).pathParam("id",1).get("api/users/{id}");
        response = request.given().baseUri(BASE_URL).pathParam("id",11).when().log().all().get("api/users/{id}");
        String responseBody = response.getBody().asString();
        System.out.println(responseBody);
        Assert.assertTrue("Response should be null!",responseBody.length()==0);
    }


    @Given("I send post request for registered user and I see that error is taken")
    public void iSendPostRequestForRegisteredUserAndISeeThatErrorIsTaken() throws Throwable{
        RestAssured.baseURI = BASE_URL;
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        int id = 12;
        response = request.when().log().all().body("{\n" +
                "  \"ID\":" + id + ",\n" +
                "  \"UserName\": \"" + userName + "\",\n" +
                "  \"Password\": \"" + passWord + "\"\n" +
                "}").post("/api/users");
        String responseBody = response.getBody().asString();
        //String responseBody = response.toString();
        boolean isContains = responseBody.contains(userName);
        Assert.assertTrue("User Duplicated!", !isContains);
    }

    @Given("I send post request with 11 digit id and I see user is registered")
    public void iSendRequestWithDigitIdAndISeeResponseIsNull() throws Throwable {
        RestAssured.baseURI = BASE_URL;
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        response = request.when().log().all().body("{\n" +
                "  \"ID\":" + ID + ",\n" +
                "  \"UserName\": \"" + userName + "\",\n" +
                "  \"Password\": \"" + passWord + "\"\n" +
                "}").post("/api/users");
        String responseBody = response.getBody().asString();
        //String responseBody = response.toString();
        boolean isContains = responseBody.contains(userName);
        Assert.assertTrue("User has not registered!", isContains);
    }

    @Given("I send post request with null id and see response is null")
    public void iSendRequestWithNullIdAndSeeResponseIsNull() throws Throwable{
        RestAssured.baseURI = BASE_URL;
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        response = request.when().log().all().body("{\n" +
                "  \"ID\":" + ""+ ",\n" +
                "  \"UserName\": \"" + userName + "\",\n" +
                "  \"Password\": \"" + passWord + "\"\n" +
                "}").post("/api/users");
        String responseBody = response.getBody().asString();
        Assert.assertTrue("Response should be null!", responseBody != null);
    }

    @Given("I send post request to register new user")
    public void iSendPostRequestToRegisterNewUser() throws Throwable {
        RestAssured.baseURI = BASE_URL;
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        response = request.when().log().all().body("{\n" +
                "  \"ID\":" + ID2 + ",\n" +
                "  \"UserName\": \"" + userName + "\",\n" +
                "  \"Password\": \"" + passWord + "\"\n" +
                "}").post("/api/users");
        String responseBody = response.getBody().asString();
        //String responseBody = response.toString();
        boolean isContains = responseBody.contains(userName);
        Assert.assertTrue("User has not registered!", isContains);
    }

    @Then("I see registered user info can be received by using get method")
    public void iSeeRegisteredUserInfoCanBeReceivedByUsingGetMethod() throws Throwable{
        RestAssured.baseURI = BASE_URL;
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        //RestAssured.given().baseUri(BASE_URL).pathParam("id",1).get("api/users/{id}");
        response = request.given().baseUri(BASE_URL).pathParam("id",ID2).get("api/users/{id}");
        String responseBody = response.getBody().asString();
        boolean isContains = responseBody.contains(userName);
        Assert.assertTrue("User info cannot be seen!", isContains);
    }
}
