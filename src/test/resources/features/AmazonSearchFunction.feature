@ui-test

  #As i do not have definition of done or the scope of this search functionality,i ve decided to test data length boundaries to observe system behaviour.

  Feature: Checking the functionality of search field

    Scenario Outline:Check search functionality by setting string length to understand boundaries
      Given I navigated to Amazon Page
      When I write "<String1>" to search textbox
      Then I see website has a response

      Examples:
      |String1|
      | a     |
      | apple |
      |appleapple|


      Scenario:Check search functionality by setting string length to understand top level of boundaries
        Given I navigated to Amazon Page
        When I write appleappleapple to search textbox
        Then I see website has nothing to show


      Scenario:Check search functionality by setting the textbox as null to understand bottom level of boundaries
        Given I navigated to Amazon Page
        When I write nothing to search textbox
        Then I see website has nothing to show and redirected to home page


