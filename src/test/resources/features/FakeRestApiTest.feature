@api-test

Feature: Checking the API service

  Scenario:Send user register request to check if new users can be registered
    Given I send post request to define a user and I see that user has been registered


  Scenario:Send get request to have information of registered user
    Given I send get request to have info of registered user

  Scenario:Send get request by using id that does not exist and check if 404 error can be taken
    Given I send get request by using id that does not exist

  Scenario:Send request for a user that is already registered and see it cannot be registered
    Given I send post request for registered user and I see that error is taken
#I've managed to duplicate users and could not find a way to receive error if there is any. Therefore, this scenario will be failed.


  Scenario:Send request to check ID param can be 10 digits and user is registered
    Given I send post request with 11 digit id and I see user is registered

  Scenario:Send request by setting id as null and check if response is null
    Given I send post request with null id and see response is null

  Scenario:Send request to register user and try to get registered user's info
    Given I send post request to register new user
    Then I see registered user info can be received by using get method
      #Registered user's info cannot be received in this case.Therefore this must be failed.
